<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


add_action('wp_footer', 'loadMapJs',99);
// If on the homepage check the window width, if it is a desktop device, load the map, if not on homepage load it always
function loadMapJs() {
    if(is_front_page()):?>

    <script id="loadInteractiveMap">
        jQuery(document).ready(function(){
            let width = jQuery(window).width();
            if (width >= 768) {
                iMaps.init(false);
            }
        })
    </script>
    <?php else:?>
        <script>
                if (typeof(iMaps) !== 'undefined') {
                    iMaps.init(false);
                    }
        </script>
    <?php endif;
}
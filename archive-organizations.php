<?php
/*  ----------------------------------------------------------------------------
    the archive(s) template
 */

get_header();


//set the template id, used to get the template specific settings
$template_id = 'archive';

//prepare the loop variables
global $loop_module_id, $loop_sidebar_position, $part_cur_auth_obj;
$loop_module_id = td_util::get_option('tds_' . $template_id . '_page_layout', 1); //module 1 is default
$loop_sidebar_position = td_util::get_option('tds_' . $template_id . '_sidebar_pos'); //sidebar right is default (empty)

// sidebar position used to align the breadcrumb on sidebar left + sidebar first on mobile issue
$td_sidebar_position = '';
if($loop_sidebar_position == 'sidebar_left') {
    $td_sidebar_position = 'td-sidebar-left';
}

//read the current author object - used by here in title and by /parts/author-header.php
$part_cur_auth_obj = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));


//prepare the archives page title
$td_archive_title = __td('Organizations', TD_THEME_NAME);
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'organizations',
    'paged' => $paged,
    'meta_key' => 'country-tip-organizations',
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'status' => 'published'
);
$theQuery = new WP_Query($args);
?>
    <div class="td-main-content-wrap td-container-wrap">
        <div class="td-container <?php echo $td_sidebar_position; ?>">
            <div class="td-crumb-container">
                <?php echo td_page_generator::get_archive_breadcrumbs(); // get the breadcrumbs - /includes/wp_booster/td_page_generator.php ?>
            </div>
            <div class="td-pb-row">
                <?php
                switch ($loop_sidebar_position) {
                    default:
                        ?>
                        <div class="td-pb-span8 td-main-content">
                            <div class="td-ss-main-content">
                                <div class="td-page-header">
                                    <h1 class="entry-title td-page-title">
                                        <span><?php echo $td_archive_title; ?></span>
                                    </h1>
                                </div>

                                <?php

                                if ($theQuery->have_posts()) {
                                    ?>
                                    <div class="td-modules-container td-module-number<?php echo $loop_module_id; ?> <?php echo $td_sidebar_position; ?>">
                                        <?php
                                        while ( $theQuery->have_posts() ) : $theQuery->the_post();
                                            echo "<div class='countryOrganizationDiv'>";
                                            echo "<h3 class='countryOrganization'>";
                                            echo get_post_meta($post->ID, "country-tip-organizations", true);
                                            echo "</h3>";
                                            echo "<h4>";
                                            echo get_the_title();
                                            echo "</h4>";
                                            echo "<p>";
                                            echo get_the_content();
                                            echo "</p>";
                                            echo "<p>";
                                            echo get_post_meta($post->ID, "url-tip-organizations", true);
                                            echo "</p>";
                                            echo "</div>";
                                        endwhile;
                                        ?>
                                    </div>
                                    <?php

                                } else {
                                    /**
                                     * no posts to display. This function generates the __td('No posts to display').
                                     * the text can be overwritten by the themplate using the global @see td_global::$custom_no_posts_message
                                     */

                                    echo td_page_generator::no_posts();
                                }
                                ?>

                                <?php echo td_page_generator::get_pagination(); // get the pagination - /includes/wp_booster/td_page_generator.php ?>
                            </div>
                        </div>

                        <div class="td-pb-span4 td-main-sidebar">
                            <div class="td-ss-main-sidebar">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                        <?php
                        break;

                    case 'sidebar_left':
                        ?>
                        <div class="td-pb-span8 td-main-content <?php echo $td_sidebar_position; ?>-content">
                            <div class="td-ss-main-content">
                                <div class="td-page-header">
                                    <h1 class="entry-title td-page-title">
                                        <span><?php echo $td_archive_title; ?></span>
                                    </h1>
                                </div>

                                <?php locate_template('loop.php', true);?>

                                <?php echo td_page_generator::get_pagination(); // get the pagination - /includes/wp_booster/td_page_generator.php ?>
                            </div>
                        </div>
                        <div class="td-pb-span4 td-main-sidebar">
                            <div class="td-ss-main-sidebar">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                        <?php
                        break;

                    case 'no_sidebar':
                        ?>
                        <div class="td-pb-span12 td-main-content">
                            <div class="td-ss-main-content">
                                <div class="td-page-header">
                                    <h1 class="entry-title td-page-title">
                                        <span><?php echo $td_archive_title; ?></span>
                                    </h1>
                                </div>
                                <?php locate_template('loop.php', true);?>

                                <?php echo td_page_generator::get_pagination(); // get the pagination - /includes/wp_booster/td_page_generator.php ?>
                            </div>
                        </div>
                        <?php
                        break;
                }
                ?>
            </div> <!-- /.td-pb-row -->
        </div> <!-- /.td-container -->
    </div> <!-- /.td-main-content-wrap -->

<?php
wp_reset_postdata();
get_footer();
